import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { ToDoComponent } from './components/to-do/to-do.component';
import { TodoFilterPipe } from './pipes/todo-filter.pipe';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoFormComponent } from './components/todo-form/todo-form.component';
import { TodoServiceService } from './services/todo-service.service';
import { RouterModule } from '@angular/router';
import { DetalleTareaComponent } from './components/detalle-tarea/detalle-tarea.component';

@NgModule({
  declarations: [
    AppComponent,
    ToDoComponent,
    TodoFilterPipe,
    TodoListComponent,
    TodoFormComponent,
    DetalleTareaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: ToDoComponent },
      { path: 'detalle/:id', component: DetalleTareaComponent }
    ])
  ],
  providers: [
    TodoServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
