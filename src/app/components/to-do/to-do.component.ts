import { Component, OnInit } from '@angular/core';
import { TodoServiceService } from 'src/app/services/todo-service.service';
import { Todo } from '../models/Todo';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {

  filtroTarea: string;

  listadoTareas: Todo[] = [];

  contadorTareas = 0;

  constructor(
    private readonly todoService: TodoServiceService
  ) { }

  ngOnInit(): void {
    this.refrescarTareas();
  }

  agregaTarea(tarea: string): void {
    if(tarea == null) {
      return;
    }

    const nuevaTarea = {
      nombre: tarea,
      fecha: new Date(Date.now()),
      completo: false,
      id: 0
    };
    this.todoService.saveData(nuevaTarea, "todo");
    this.refrescarTareas();
  }

  refrescarTareas(){
    this.listadoTareas = this.todoService.getTareas("todo");
  }

  completarTarea(idTarea: number): void {
    this.listadoTareas = this.listadoTareas.map(
      (tarea) => {
        if (idTarea == tarea.id){
          tarea.completo = true;
        }

        return tarea;
      }
    )
  }

  completar(idTarea: number): void {
    this.cambiarEstado(idTarea, true);
  }

  restaurar(idTarea: number): void {
    this.cambiarEstado(idTarea, false);
  }

  cambiarEstado(idTarea: number, estado: boolean): void {
    this.todoService.cambiarEstadoTarea(idTarea, estado);
    this.refrescarTareas();
  }

  removerTarea(idTarea: number): void {
    this.listadoTareas = this.listadoTareas.filter(
      (tarea) => tarea.id !== idTarea
    );
  }
}
