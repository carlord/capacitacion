import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {

  @Output() agregarTareaEvent = new EventEmitter<string>();

  tarea: string;

  constructor() { }

  ngOnInit(): void {
  }

  agregaTarea() {
    this.agregarTareaEvent.emit(this.tarea);
    this.tarea = "";
  }

}
