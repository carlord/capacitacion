import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../models/Todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  @Input() listadoTareas: Todo[];

  filtroTareas: string;

  @Output() completarTareaEvent = new EventEmitter<number>();
  @Output() restaurarTareaEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  completarTarea(tarea) {
    this.completarTareaEvent.emit(tarea.id);
  }

  restaurar(tarea) {
    this.restaurarTareaEvent.emit(tarea.id)
  }

}
