import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TodoServiceService } from 'src/app/services/todo-service.service';
import { Todo } from '../models/Todo';

@Component({
  selector: 'app-detalle-tarea',
  templateUrl: './detalle-tarea.component.html',
  styleUrls: ['./detalle-tarea.component.scss']
})
export class DetalleTareaComponent implements OnInit {

  currentTodo = new Todo();

  detalleTodo = this.formBuilder.group({
    responsable: ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(2)])],
    esfuerzo: ['', Validators.required],
    prioridad: ['', Validators.required],
    descripcion: ['', Validators.required],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly todoService: TodoServiceService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getRouteData();
  }

  getRouteData() {
    const idTodo = this.activatedRoute.snapshot.params.id;
    this.currentTodo = this.todoService.getById(idTodo);

  }

  complementar() {
    this.detalleTodo.markAllAsTouched();
  }

}
