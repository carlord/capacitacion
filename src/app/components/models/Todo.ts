
export class Todo {
  nombre: string;

  fecha: Date;

  completo: boolean;

  id: number;
}
