import { Injectable } from '@angular/core';
import { Todo } from '../components/models/Todo';

@Injectable()
export class TodoServiceService {

  readonly storageKey = "todo";

  listadoTareas: any[];

  constructor() { }

  saveData(tarea: Todo, key: string) : void {
    const tareas = this.getTareas(key);

    tarea.id = tareas.length == 0 ? 1 : tareas.length + 1;
    tareas.push(tarea);
    localStorage.setItem(this.storageKey, JSON.stringify(tareas));
  }

  getTareas(key: string): Todo[] {
    const todoJson = localStorage.getItem(key);

    this.listadoTareas = todoJson !== undefined && todoJson !== null ?
      JSON.parse(todoJson): [];

    return this.listadoTareas;
  }

  getById(id: number) {
    const tareas =this.getTareas(this.storageKey);
    return tareas.find(m => m.id == id);
  }

  getlistado() {
    return this.listadoTareas;
  }

  cambiarEstadoTarea(idTarea: number, estado: boolean): void {
    const tareas =this.getTareas(this.storageKey);
    const listadoTareas = tareas.map(m => {
      if (m.id == idTarea){
        m.completo = estado;
      }
      return m;
    });
    localStorage.setItem(this.storageKey, JSON.stringify(listadoTareas));
  }

  resetList() {
    this.listadoTareas = [];
  }
}
