import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from '../components/models/Todo';

@Pipe({
  name: 'todoFilter'
})
export class TodoFilterPipe implements PipeTransform {

  transform(value: Todo[], filtro: string): unknown {
    if (filtro == null || filtro.length == 0 ){
      return value;
    }

    return value.filter(m => m.nombre.toUpperCase().includes(filtro.toUpperCase()));
  }

}
